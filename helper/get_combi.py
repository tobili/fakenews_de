import file
import nltk
import pprint

dataset = 'test'  # test or train

bodies = file.read('../data/de/backup/negation/{}_bodies_only_neg.csv'.format(dataset))
stances = file.read('../data/de/backup/negation/{}_stances_only_neg.csv'.format(dataset))

bodies_dict = {}
for body in bodies:
    body_id = body['Body ID']
    body_text = body['articleBody'].lower()
    bodies_dict.update({body_id: body_text})

combinations = []
label_count = dict({
    'agree': {'neg_body': 0, 'neg_head': 0, 'overlap': 0},
    'disagree': {'neg_body': 0, 'neg_head': 0, 'overlap': 0},
    'discuss': {'neg_body': 0, 'neg_head': 0, 'overlap': 0},
    'unrelated': {'neg_body': 0, 'neg_head': 0, 'overlap': 0}})
for stance in stances:
    label = stance['Stance']
    stance_text = stance['Headline'].lower()

    neg_head = set(nltk.word_tokenize(stance_text))
    neg_body = set(nltk.word_tokenize(bodies_dict[stance['Body ID']]))
    overlap = neg_body & neg_head
    if overlap:
        label_count[label]['overlap'] += 1  # len(overlap)
    if len(neg_head):
        label_count[label]['neg_head'] += 1  # len(neg_head)
    if len(neg_body):
        label_count[label]['neg_body'] += 1  # len(neg_body)
    print(len(neg_body), len(neg_head), len(overlap), label)
    combinations.append(
        {'Neg_body': len(neg_body), 'Neg_headline': len(neg_head), 'Neg_Overlap': len(overlap), 'Stance': label})

file.write('../data/de/backup/negation/{}_neg_only_diff.csv'.format(dataset), combinations,
           ['Neg_body', 'Neg_headline', 'Neg_Overlap', 'Stance'])
pp = pprint.PrettyPrinter(indent=4)
pp.pprint(label_count)
