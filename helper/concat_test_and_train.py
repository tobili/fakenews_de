import file
import testset


def get_all_stances(b_id, stances):
    result_stances = []
    for s in stances:
        if s['Body ID'] == b_id:
            result_stances.append(s)
    return result_stances


test_bodies = file.read('../data/de/test_bodies.csv')
test_stances = file.read('../data/de/test_stances.csv')

testset.body_path = '../data/de/crossfold/train_bodies.csv'
testset.stance_gold_path = '../data/de/crossfold/train_stances.csv'

for body in test_bodies:
    body_id = testset.add_to_body(body['articleBody'])
    all_stances = get_all_stances(body['Body ID'], test_stances)
    for stance in all_stances:
        testset.add_to_stance(body_id, stance['Headline'], stance['Stance'])
