import file
import normalize
import nltk

test_train = 'train'  # test or train

train_bodies = file.read('../data/de/{}_bodies.csv'.format(test_train))
train_stances = file.read('../data/de/{}_stances.csv'.format(test_train))

num_bodies = len(train_bodies)
new_train_bodies = []
new_train_stances = []

for train_body in train_bodies:
    train_body['articleBody'] = ' '.join(
        normalize.normalize(w, lang='de') for w in nltk.word_tokenize(train_body['articleBody']) if
        normalize.normalize(w, lang='de'))
    new_train_bodies.append(train_body)

for train_stance in train_stances:
    train_stance['Headline'] = ' '.join(normalize.normalize(w, lang='de') for w in
                                        nltk.word_tokenize(train_stance['Headline']) if
                                        normalize.normalize(w, lang='de'))
    new_train_stances.append(train_stance)

file.write('../data/de/normal/{}_bodies.csv'.format(test_train), new_train_bodies, ['Body ID', 'articleBody'])
file.write('../data/de/normal/{}_stances.csv'.format(test_train), new_train_stances, ['Headline', 'Body ID', 'Stance'])
