# randomize the order of entries in dataset

import file
import random

path = '../data/de/crossfold/train_stances.csv'

data = file.read(path)

r = list(range(len(data)))
random.shuffle(r)

random_data = []
for i in r:
    random_data.append(data[i])

file.write(path, random_data, ['Headline', 'Body ID', 'Stance'])
