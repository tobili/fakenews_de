import file
import json

source_file_name = 'train_stances'
is_stance = True

dest_file_name = source_file_name + '_only_neg'
main_path = '../data/de/'
source_csv_path = '{}{}.csv'.format(main_path, source_file_name)


def percentage(current_val, max_val, text='', debug=False):
    percent = (current_val / max_val) * 100
    print("   {0:.3f}% ".format(percent), text, end='\r', flush=True)


def get_negation_words(txt_id):
    raw_file = file.read_txt('{}txt/{}/json/{}.txt.json'.format(main_path, source_file_name, txt_id))
    json_file = json.loads(raw_file)
    sentences = json_file['sentences']
    neg_words = ''
    for sentence in sentences:
        for word in sentence['enhancedPlusPlusDependencies']:
            if word['dep'] == 'neg':
                neg_words += ' ⫮' + str(word['governorGloss'])  # ⫮ is the negation Symbol
    return neg_words


if __name__ == '__main__':

    source_file = file.read(source_csv_path)

    result = []
    fieldnames = None
    for i, line in enumerate(source_file):
        percentage(i, len(source_file), 'of file done')
        if is_stance:
            file_id = i
            text = line['Headline']
            negations = get_negation_words(file_id)
            result.append({'Headline': negations, 'Body ID': line['Body ID'], 'Stance': line['Stance']})
            fieldnames = ['Headline', 'Body ID', 'Stance']
        else:
            file_id = line['Body ID']
            text = line['articleBody']
            negations = get_negation_words(file_id)
            result.append({'Body ID': line['Body ID'], 'articleBody': negations})
            fieldnames = ['Body ID', 'articleBody']

    file.write('{}{}.csv'.format(main_path, dest_file_name), result, fieldnames)
