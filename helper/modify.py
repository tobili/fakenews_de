# modify the de train dataset. remove 10% of all entries in train_bodies.csv
# and the linked stances in train_stances.csv.
# More details "in 3.4.5 Trainingsdaten filtern" in PDF

import file
import sys


def get_all_stances(body_id):
    result_stances = []
    for stance in train_stances:
        if stance['Body ID'] == body_id:
            result_stances.append(stance)
    return result_stances


testsplit = 30  # percentage

train_bodies = file.read('../data/de/normal/train_bodies.csv')
train_stances = file.read('../data/de/normal/train_stances.csv')

num_bodies = len(train_bodies)
new_test_bodies = []
new_test_stances = []
new_train_bodies = []
new_train_stances = []

train_bodies_id = []
test_bodies_id = []

for i, train_body in enumerate(train_bodies):
    if i < (num_bodies * testsplit / 100):
        # test data
        test_bodies_id.append(train_body['Body ID'])
        new_test_bodies.append(train_body)
        ([new_test_stances.append(stance) for stance in get_all_stances(train_body['Body ID'])])
    else:
        # train data
        train_bodies_id.append(train_body['Body ID'])
        new_train_bodies.append(train_body)
        ([new_train_stances.append(stance) for stance in get_all_stances(train_body['Body ID'])])

if set(train_bodies_id).intersection(set(test_bodies_id)):
    sys.exit('Body ID in train AND test')


out_path = '../data/de/normal/modified/'
file.write('{}train_bodies.csv'.format(out_path), new_train_bodies, ['Body ID', 'articleBody'])
file.write('{}train_stances.csv'.format(out_path), new_train_stances, ['Headline', 'Body ID', 'Stance'])

file.write('{}test_bodies.csv'.format(out_path), new_test_bodies, ['Body ID', 'articleBody'])
file.write('{}test_stances.csv'.format(out_path), new_test_stances, ['Headline', 'Body ID', 'Stance'])
