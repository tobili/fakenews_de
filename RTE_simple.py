# Recognizing Textual Entailment - simple
import normalize


class RTEFeatureExtractor(object):
    """
    This builds a bag of words for both the text and the hypothesis after
    throwing away some stopwords, then calculates overlap and difference.
    """

    def __init__(self, _text, _hyp, normal=True, lang='en'):
        """
        :param _text: a string with the long text
        :type _text: string
        :param _hyp: a string with the hypothesis
        :type _hyp: string
        :param normal: if ``True``, normalizing the words.
        :type normal: bool
        """
        self.normal = normal
        self.negwords = {
            'bogus',
            'debunk',
            'denied',
            'deny',
            'denies',
            'despite',
            'doubt',
            'doubts',
            'failed',
            'fake',
            'false',
            'fraud',
            'hoax',
            'never',
            'no',
            'not',
            'nope',
            'pranks',
            'retract',
            'rejected',
            'nicht',  # german negwords
            'nichts',
            'nein',
            'nie',
            'niemals',
            'nirgends',
            'nirgendwo',
            'nirgendwohin',
            'niemand',
            'niemanden',
            'kein',
            'keine',
            'keinen',
            'keinem',
            'keines',
            'keiner',
            'keinesfalls',
            'keineswegs'
            'falschmeldung',
            'ente',
            'gefälscht',
            'falsch',
            'unecht',
            'witz',
            'scherz',
            'falsch',
            'erfunden',
            'erfand',
            'weder',  # ...noch
            'dementiert',
            'dementieren',
            'verneint'

        }
        # Try to tokenize so that abbreviations, monetary amounts, email
        # addresses, URLs are single tokens.
        from nltk.tokenize import RegexpTokenizer
        tokenizer = RegexpTokenizer('[\w.@:/]+|\w+|\$[\d.]+')

        # Get the set of word types for text and hypothesis
        self.text_tokens = tokenizer.tokenize(_text)
        self.hyp_tokens = tokenizer.tokenize(_hyp)
        self.text_words = set(self.text_tokens)
        self.hyp_words = set(self.hyp_tokens)

        if self.normal:
            self.text_words = {normalize.normalize(a, lang=lang) for a in self.text_words} - {None}
            self.hyp_words = {normalize.normalize(a, lang=lang) for a in self.hyp_words} - {None}

        self._overlap = self.hyp_words & self.text_words
        self._hyp_extra = self.hyp_words - self.text_words
        self._txt_extra = self.text_words - self.hyp_words

    def overlap(self, toktype, debug=False):
        """
        Compute the overlap between text and hypothesis.

        :param toktype: distinguish Named Entities from ordinary words
        :type toktype: 'ne' or 'word'
        """
        ne_overlap = set(token for token in self._overlap if ne(token))
        if toktype == 'ne':
            if debug:
                print("ne overlap", ne_overlap)
            return ne_overlap
        elif toktype == 'word':
            if debug:
                print("word overlap", self._overlap - ne_overlap)
            return self._overlap - ne_overlap
        else:
            raise ValueError("Type not recognized:'%s'" % toktype)

    def hyp_extra(self, toktype, debug=True):
        """
        Compute the extraneous material in the hypothesis.

        :param toktype: distinguish Named Entities from ordinary words
        :type toktype: 'ne' or 'word'
        """
        ne_extra = set(token for token in self._hyp_extra if ne(token))
        if toktype == 'ne':
            return ne_extra
        elif toktype == 'word':
            return self._hyp_extra - ne_extra
        else:
            raise ValueError("Type not recognized: '%s'" % toktype)


def ne(token):
    """
    This just assumes that words in all caps or titles are
    named entities.

    :type token: str
    """
    if token.istitle() or token.isupper():
        return True
    return False


def create_RTE_features(_text, _hyp, lang='en'):
    extractor = RTEFeatureExtractor(_text, _hyp, lang=lang)
    features = {}
    features['word_overlap'] = len(extractor.overlap('word'))
    features['word_hyp_extra'] = len(extractor.hyp_extra('word'))
    # features['ne_overlap'] = len(extractor.overlap('ne'))
    # features['ne_hyp_extra'] = len(extractor.hyp_extra('ne'))
    features['neg_txt'] = len(extractor.negwords & extractor.text_words)
    features['neg_hyp'] = len(extractor.negwords & extractor.hyp_words)

    neg_verb_txt = [w for w in extractor.text_words if '⫮' in w]
    features['neg_verb_txt'] = len(neg_verb_txt)
    neg_verg_hyp = [w for w in extractor.hyp_words if '⫮' in w]
    features['neg_verb_hyp'] = len(neg_verg_hyp)
    features['neg_verb_overlap'] = len(set(neg_verb_txt) & set(neg_verg_hyp))

    features['neg_overlap'] = len((extractor.negwords & extractor.text_words) & (extractor.negwords & extractor.hyp_words))
    features['neg_hyp_extra'] = len((extractor.negwords & extractor.hyp_words) - (extractor.negwords & extractor.text_words) )
    features['neg_hyp_diff'] = len(extractor.negwords & extractor.hyp_words) - len(extractor.negwords & extractor.text_words)

    return features
