import re

import requests
from bs4 import BeautifulSoup

import file

lang = 'de'
body_path = 'data/{}/test_bodies.csv'.format(lang)
stance_gold_path = 'data/{}/test_stances.csv'.format(lang)


def add_row(path, text):
    with open(path, "a", encoding="UTF-8") as file:
        file.write(text)
        file.close()


def get_last_body_id():
    with open(body_path, "r", encoding="UTF-8") as file:
        last_lines = file.readlines()[-1]
        body_id = last_lines.split(',')[0]
        file.close()
        return body_id


def add_to_body(body=None):
    if not body:
        print('----------------------')
        print('new item, enter body string:')
        body = '\n'.join(iter(input, '#END'))
    body_clean = body.replace('"', '“').replace('\n', ' ')
    body_clean = re.sub(r'\s\s+', ' ', body_clean)
    print(body_clean)
    body_id = str(int(get_last_body_id()) + 1)
    add_row(body_path, '{},"{}"\n'.format(body_id, body_clean))
    return body_id


def add_to_stance(body_id=None, headline=None, stance=None):
    if not headline:
        print('------------------------')
        print('add a new item to stance')
        headline = input('enter headline: ')
    headline_clean = headline.replace('"', '“').replace('\n', ' ')
    headline_clean = re.sub(r'\s\s+', ' ', headline_clean)
    if not body_id:
        body_id = input('enter body id: ')
    if not stance:
        stance = input('enter stance: ')
    print(headline)
    add_row(stance_gold_path, '"{}",{},{}\n'.format(headline_clean, body_id, stance))


def add_combi():
    print('add a new item to stance')
    headline = input('enter headline: ')
    body_id = add_to_body()
    add_to_stance(body_id=body_id, headline=headline)


def count_labels(path):
    source_file = file.read(path)
    counter = {}
    for line in source_file:
        if line['Stance'] in counter:
            counter[line['Stance']] += 1
        else:
            counter[line['Stance']] = 1
    print(counter)


def save_from_url(url):
    r = requests.get(url)
    html = BeautifulSoup(r.text, 'html5lib')
    headline = html.findAll('span', class_='dh1 head5')[0].text
    paragraphes = html.find('div', class_='formatted').find_all('p')
    body = ''
    for p in paragraphes:
        p_class = p.get('class') if p.get('class') else ''
        if 'anzeige' not in p_class:
            body += p.text
    print(body)
    body_id = add_to_body(body)
    add_to_stance(body_id, headline, 'agree')


if __name__ == "__main__":
    while True:
        input_type = input('add body, stance, combi? (body/stance/combi)')

        if input_type == 'body':
            add_to_body()
        elif input_type == 'stance':
            add_to_stance()
        elif input_type == 'combi':
            add_combi()

        count_labels(stance_gold_path)

    while False:
        input_url = input('enter URL (https://www.news.com/article.html):')
        save_from_url(input_url)
        count_labels(stance_gold_path)
