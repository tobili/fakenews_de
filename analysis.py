import file
import nltk
import collections
import re


def read_files(body_path, stance_path):
    """
    read the body and stance csv and return bodies adn bodies as a ordered dict and bodies_dict as dict
    :param body_path: path to body file
    :param stance_path: path to stance file
    :return: bodies, bodies_dict, bodies
    """
    bodies = file.read(body_path)
    bodies_dict = {}
    for b in bodies:
        bodies_dict.update({b['Body ID']: b['articleBody']})
    stances = file.read(stance_path)

    return bodies, bodies_dict, stances


def count_labels(_stances):
    """
    count ammount of labels in given orderedDict
    :param _stances:
    :return:
    """
    labels = {}
    for stance in _stances:
        if stance['Stance'] in labels:
            labels[stance['Stance']] += 1
        else:
            labels.update({stance['Stance']: 1})
    return labels


def percentage_labels(_stances):
    all_labels = count_labels(_stances)
    len_labels = sum(n for n in all_labels.values())
    labels = {}
    for k, v in all_labels.items():
        labels.update({k: round((v / len_labels * 100), 3)})
    return labels


def tokenize_lines(text_orderd_dict, type='body', label_filter=None):
    """
    :param text_orderd_dict:
    :param type:
    :param label_filter: list of labels which should be returned
    :return:
    """
    words = []
    for line in text_orderd_dict:
        if type == 'headline':
            if label_filter and line['Stance'] in label_filter or not label_filter:
                words += nltk.word_tokenize(line['Headline'])
        else:
            words += nltk.word_tokenize(line['articleBody'])
    return words


def count_words(word_list):
    """
    filter stopwords and return collection of wourdcount
    :param word_list: list of words to count
    :type word_list: list
    :return: collection
    """
    stop = set(nltk.corpus.stopwords.words('german'))
    return collections.Counter(word for word in word_list if word.lower() not in stop and re.match(r'\w', word))


if __name__ == "__main__":
    test_bodies, test_bodies_dict, test_stances = read_files('data/de/test_bodies.csv', 'data/de/test_stances.csv')
    train_bodies, train_bodies_dict, train_stances = read_files('data/de/train_bodies.csv', 'data/de/train_stances.csv')

    print(count_labels(test_stances))
    print("original label in fnc1 dataset: {'unrelated': 72.729, 'agree': 7.423, 'disagree': 1.701, 'discuss': 18.147}")
    print(percentage_labels(test_stances))
    test_words_bodies = tokenize_lines(test_bodies)
    train_words_bodies = tokenize_lines(train_bodies)
    test_words_headline = tokenize_lines(test_stances, 'headline')
    train_words_headline = tokenize_lines(train_stances, 'headline')
    print('words in discuss: ', len(test_words_headline))
    test_words_all = test_words_bodies + test_words_headline

    most_common = count_words(test_words_all)
    print(most_common.most_common(10))
    print(len(most_common))

    BoWs = file.read_txt('data/de/bow_tokens.txt')

    combined_body = [w for w in test_words_bodies if w.lower() in BoWs]
    combined_headline = [w for w in test_words_headline if w.lower() in BoWs]

    print('wörter in vektor und Datenset:' , len(combined_body) , ' Wörter in Datenset: ', len(test_words_bodies), ' in Prozent: ', str(len(combined_body)/len(test_words_bodies)*100))

    # __, bodies_dict, _ = read_files('data/de/train_bodies.csv', 'data/de/train_stances.csv')
    # with open('data/de/train_pairs.csv', "r+", encoding="UTF-8") as dest_file:
    #     lines = dest_file.readlines()
    #     dest_file.seek(0)
    #     dest_file.truncate()
    #     for line in lines[1:]:
    #         splitted_line = line.split(',')
    #         body_id = None
    #         try:
    #             body_id = int(splitted_line[1])
    #         except:
    #             try:
    #                 body_id = int(splitted_line[2])
    #             except:
    #                 body_id = int(splitted_line[3])
    #         body = bodies_dict[str(body_id)]
    #         result = '' + splitted_line[0] +',"'+ body.replace('"','“') +'",'+ splitted_line[2] +'\n'
    #         dest_file.write(result)
    #     dest_file.close()
