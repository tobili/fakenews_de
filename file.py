import csv
import os


def read(file_path):
    print('start reading file: {}'.format(file_path))
    with open(file_path, "r", encoding="UTF-8") as file:
        reader = csv.DictReader(file)
        rows = []
        for row in reader:
            rows.append(row)
        return rows


def write(file_path, content_list, fieldnames):
    print('start writing file: {}'.format(file_path))
    if os.path.isfile(file_path):
        print('deleting old file')
        os.remove(file_path)

    with open(file_path, 'w', encoding="UTF-8") as file:
        print('creating new csv file')
        writer = csv.DictWriter(file, fieldnames=fieldnames)

        writer.writeheader()
        for line in content_list:
            content = {}
            for i in range(0, len(fieldnames)):
                try:
                    content[fieldnames[i]] = line[i]
                except:
                    content[fieldnames[i]] = line[fieldnames[i]]

            writer.writerow(content)


def save_txt(path, text):
    with open(path, 'w', encoding="UTF-8") as file:
        file.write(text)


def read_txt(path):
    with open(path, 'r', encoding="UTF-8") as filee:
        return filee.read()
