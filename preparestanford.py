import file
import os

# simple version for working with CWD

dir_name = 'test_bodies'
DIR = '/Users/tobiaslinkohr/FH_Projekte/thesis/fakenews_de/data/de/txt/{}/'.format(dir_name)
file_names = [name for name in os.listdir(DIR) if os.path.isfile(os.path.join(DIR, name))]

result = ''
for name in file_names:
    result += '{}{}\n'.format(DIR, name)
file.save_txt('{}filelist.txt'.format(DIR), result)
