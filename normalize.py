from functools import lru_cache
from num2words import num2words
import nltk
from nltk.corpus import stopwords

ones = ["", "one ", "two ", "three ", "four ", "five ",
        "six ", "seven ", "eight ", "nine "]
tens = ["ten ", "eleven ", "twelve ", "thirteen ", "fourteen ",
        "fifteen ", "sixteen ", "seventeen ", "eighteen ", "nineteen "]
twenties = ["", "", "twenty ", "thirty ", "forty ",
            "fifty ", "sixty ", "seventy ", "eighty ", "ninety "]
thousands = ["", "thousand ", "million ", "billion ", "trillion ",
             "quadrillion ", "quintillion ", "sextillion ", "septillion ", "octillion ",
             "nonillion ", "decillion ", "undecillion ", "duodecillion ", "tredecillion ",
             "quattuordecillion ", "quindecillion", "sexdecillion ", "septendecillion ",
             "octodecillion ", "novemdecillion ", "vigintillion "]
stop_en = set(stopwords.words('english'))
stop_de = set(stopwords.words('german'))
no_puncts = str.maketrans("", "", '.,?!"\'')

# init cache for stemming
wnl = nltk.stem.SnowballStemmer('german')
stemming = lru_cache(maxsize=50000)(wnl.stem)


def normalize(word, stem=True, lang='en'):
    word = word.lower()  # duration 3sec
    word = remove_punctuation(word)  # duration 7sec
    word = num_to_word(word, lang)  # duration 5sec
    word = remove_stopword(word, lang=lang)  # duration 2sec
    if word and stem:
        word = stemming(word)  # duration 4 sec

    return word


def num_to_word(n, lang):
    """
    convert an integer or string number n into a string of english words

    source: www.daniweb.com/software-development/python/code/216839

    :param n: string or integer of a number
    :type n: string or integer
    :return: string of words
    :rtype: string
    """
    n = str(n)
    if '.' in n:
        numbers = n.split('.')
        return num_to_word(numbers[0], lang) + 'point ' + num_to_word(numbers[1], lang)
    elif ',' in n:
        n = n.replace(',', '')
    if n.isdigit():
        return num2words(n, lang=lang)
    else:
        return n


def remove_punctuation(word):
    """
    remove all '.,?!"\'' in a given word

    :param word: string of one word
    :type word: string
    :return: string of word
    :rtype: string
    """

    return word.translate(no_puncts)


def remove_stopword(word, lang='en'):
    """
    remove the word if it's a english stopword

    :param word: string of one word
    :type word: string
    :param lang: language string 'en' or 'de'
    :type word: string
    :return: string of word
    :rtype: string
    """
    stop = stop_de if lang == 'de' else stop_en
    if word not in stop_en:
        return word
