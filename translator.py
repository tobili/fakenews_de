import os
import sys

import requests
import time

import file
from selenium import webdriver

service = 'google'  # 'google' OR 'yandex' - google is default
source_path = 'data/en/train_stances.csv'
destination_path = 'data/de/train_stances.csv'

yandex_key = 'trnsl.1.1.20170718T142503Z.ccfd0f623e657a75.e6d90e5ba71ef0b46d769b4ef5d37af994152797'
url_yandex = 'https://translate.yandex.net/api/v1.5/tr.json/translate'
url_google = 'https://translate.google.com/'

source_file = file.read(source_path)
results = []
delete_file = input('deleting old file, are you sure? (y/n): ')
file_exists = False
start_line = 0

# google translator
driver = webdriver.PhantomJS()  # or add to your PATH
driver.set_window_size(1024, 768)  # optional
driver.get(url_google)
text_input = driver.find_element_by_css_selector('textarea.goog-textarea.short_text')
translate_btn = driver.find_element_by_css_selector('input#gt-submit.jfk-button.jfk-button-action')
result = driver.find_element_by_css_selector('span#result_box.short_text')

empty_string = '                                                                                '


def translate_yandex(text):
    lang = 'en-de'
    data = {'key': yandex_key, 'text': text, 'lang': lang}
    r = requests.get(url_yandex, data=data)
    response = r.json()
    if r.status_code != 200:
        print('\n', response['message'])
        r.raise_for_status()
    return str(response['text'][0])


def translate_google(text):
    text_input.send_keys(str(text))
    translate_btn.click()
    starttime = time.time()
    while result.text == '' or 'Wird übersetzt...' in result.text:
        print('.', end=" ", flush=True)
        if (starttime + 10) < time.time():
            break
    driver.save_screenshot('screen.png')  # save a screenshot to disk
    delete_input_bnt = driver.find_element_by_css_selector('div#gt-clear.clear-button.goog-toolbar-button')
    ret_val = result.text
    delete_input_bnt.click()
    return ret_val


if delete_file == 'y':
    if os.path.isfile(destination_path):
        print('deleting old file')
        os.remove(destination_path)

if os.path.isfile(destination_path):
    file_exists = True

    # check if last_line row is hint for interrupted previous startet translator
    with open(destination_path, "r+", encoding="UTF-8") as dest_file:
        lines = dest_file.readlines()
        dest_file.seek(0)
        dest_file.truncate()
        for line in lines:
            if '#STOPPET AT' in str(line):
                start_line = int(line.split()[-1])
                # remove last line
                line = ''
            dest_file.write(line)
        dest_file.close()

with open(destination_path, "a", encoding="UTF-8") as dest_file:
    if not file_exists:
        dest_file.write('Headline,Body ID,Stance\n')
    translate = translate_yandex if service == 'yandex' else translate_google

    for i, line in enumerate(source_file[start_line:]):
        try:
            en_text = str(line['Headline'])
            de_text = ''
            if len(en_text) > 5000:
                de_text = translate(en_text[:5000]) + ' ' \
                          + translate(en_text[5000:10000]) + ' ' \
                          + translate(en_text[10000:15000])
            else:
                de_text = translate_google(en_text)
            de_text_clean = str(de_text).replace('"', '“')

            # progress
            percent = (i + start_line / len(source_file)) * 100
            print("   {0:.3f}% - ".format(percent), de_text_clean[:200], empty_string, end='\r')

            # write results
            dest_file.write('"{}",{},{}\n'.format(de_text_clean, line['Body ID'], line['Stance']))
        except Exception as e:
            dest_file.write('#STOPPET AT {}'.format(i + start_line))
            sys.exit('ERROR {}'.format(sys.exc_info()))
    dest_file.close()

print('\n Finished')
