# Copyright 2017 Benjamin Riedel
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Import relevant packages and modules
from util import *
import random
import tensorflow as tf
import scorer
import file
import datetime
import plotter

# Prompt for mode
mode = input('mode (load / train)? ')

subdirectory = ''  # any directory name in data/de oder data/en

now = datetime.datetime.now()
# Set file names
train_lang = 'de'  # 'de' OR 'en'
test_lang = 'de'  # 'de' OR 'en'
file_train_instances = "data/{}{}/train_stances.csv".format(train_lang, subdirectory)
file_train_bodies = "data/{}{}/train_bodies.csv".format(train_lang, subdirectory)
file_test_instances = "data/{}{}/test_stances.csv".format(test_lang, subdirectory)
file_test_bodies = "data/{}{}/test_bodies.csv".format(test_lang, subdirectory)
file_predictions = 'data/predictions_test.csv'

filename_plot_data = '{}_{}-{}-{}'.format(test_lang, now.day, now.hour, now.minute)
file_plot_data = 'data/plot_data/{}.csv'.format(filename_plot_data)

note = 'optimized DE'

# Initialise hyperparameters
r = random.Random()
lim_unigram = 42624
target_size = 4
hidden_size = 100
train_keep_prob = 0.6
l2_alpha = 0.00001
learn_rate = 0.01
clip_ratio = 5
batch_size_train = 500
epochs = 90
test_split = 0  # % of dataset for testing, 0 if test_stances should be used
crossfolds = 1  # 1 to turn off

save_data_for_plot = False

overall_results = []
for crossfold in range(crossfolds):
    # Load data sets
    raw_train = None
    raw_test = None
    if test_split == 0 and crossfolds == 1:
        raw_train = FNCData(file_train_instances, file_train_bodies)
        raw_test = FNCData(file_test_instances, file_test_bodies)
        # build split csv
        file.write('data/train.csv', file.read(file_train_instances), ['Headline', 'Body ID', 'Stance'])
        file.write('data/test.csv', file.read(file_test_instances), ['Headline', 'Body ID', 'Stance'])
        file.write('data/body.csv', file.read(file_test_bodies), ['Body ID', 'articleBody'])
    elif crossfolds == 1:
        raw_train = FNCData(file_train_instances, file_train_bodies, test_split * (-1), 0)
        raw_test = FNCData(file_train_instances, file_train_bodies, 0, test_split)
        # build split csv
        all_inst = file.read(file_train_instances)
        num_inst = int(len(all_inst) * (test_split / 100))
        file.write('data/train.csv', all_inst[:(100 - num_inst)], ['Headline', 'Body ID', 'Stance'])
        file.write('data/test.csv', all_inst[-num_inst:], ['Headline', 'Body ID', 'Stance'])
        file.write('data/body.csv', file.read(file_train_bodies), ['Body ID', 'articleBody'])
    else:
        num_inst = len(file.read(file_train_instances))
        split_end = (crossfold + 1) * (100 // crossfolds)
        split_start = crossfold * (100 // crossfolds)

        percentage_end = int((num_inst / crossfolds) * (crossfold + 1))
        percentage_start = percentage_end - (num_inst // crossfolds)

        raw_train = FNCData(file_train_instances, file_train_bodies, split_start, split_end, inverse_split=True)
        raw_test = FNCData(file_train_instances, file_train_bodies, split_start, split_end)

        all_inst = file.read(file_train_instances)
        file.write('data/train.csv', all_inst[:percentage_start] + all_inst[percentage_end:], ['Headline', 'Body ID', 'Stance'])
        file.write('data/test.csv', all_inst[percentage_start:percentage_end], ['Headline', 'Body ID', 'Stance'])
        file.write('data/body.csv', file.read(file_train_bodies), ['Body ID', 'articleBody'])

    n_train = len(raw_train.instances)

    # Process data sets
    train_set, train_stances, bow_vectorizer, tfreq_vectorizer, tfidf_vectorizer = \
        pipeline_train(raw_train, raw_test, lim_unigram=lim_unigram, lang=train_lang)
    feature_size = len(train_set[0])
    test_set = pipeline_test(raw_test, bow_vectorizer, tfreq_vectorizer, tfidf_vectorizer, lang=test_lang)
    _, test_stances, _, _, _ = \
        pipeline_train(raw_test, raw_train, lim_unigram=lim_unigram, lang=test_lang)

    # save vector
    feature_names = bow_vectorizer.get_feature_names()
    file.save_txt('data/de/bow_tokens.txt', str(feature_names))
    # Define model

    # Create placeholders
    features_pl = tf.placeholder(tf.float32, [None, feature_size], 'features')
    stances_pl = tf.placeholder(tf.int64, [None], 'stances')
    keep_prob_pl = tf.placeholder(tf.float32, name='keep_prop')

    # Infer batch size
    batch_size = tf.shape(features_pl)[0]

    # Define multi-layer perceptron
    hidden_layer = tf.nn.dropout(
        tf.nn.relu(tf.contrib.layers.linear(features_pl, hidden_size)),
        keep_prob=keep_prob_pl,
        name='hidden_layer')
    logits_flat = tf.nn.dropout(
        tf.contrib.layers.linear(hidden_layer, target_size),
        keep_prob=keep_prob_pl,
        name='labels')
    logits = tf.reshape(logits_flat, [batch_size, target_size])

    # Define L2 loss
    tf_vars = tf.trainable_variables()
    l2_loss = tf.add_n([tf.nn.l2_loss(v) for v in tf_vars if 'bias' not in v.name]) * l2_alpha

    # Define overall loss
    loss = tf.reduce_sum(tf.nn.sparse_softmax_cross_entropy_with_logits(logits, stances_pl) + l2_loss)

    # Define prediction
    softmaxed_logits = tf.nn.softmax(logits)
    predict = tf.arg_max(softmaxed_logits, 1)

    test_pred = None
    # Load model
    if mode == 'load':
        with tf.Session() as sess:
            load_model(sess)

            # Predict
            test_feed_dict = {features_pl: test_set, keep_prob_pl: 1.0}
            test_pred = sess.run(predict, feed_dict=test_feed_dict)

    plot_data = None
    if save_data_for_plot:
        plot_data = open(file_plot_data, 'a')
        plot_data.writelines("Epoch,TestLoss,TrainLoss,TestAccuracy,TrainAccuracy,TestScore,TrainScore")

    # Train model
    if mode == 'train':

        # Define optimiser
        opt_func = tf.train.AdamOptimizer(learn_rate)
        grads, _ = tf.clip_by_global_norm(tf.gradients(loss, tf_vars), clip_ratio)
        opt_op = opt_func.apply_gradients(zip(grads, tf_vars))

        # Perform training
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            # write model graph
            # summary_writer = tf.summary.FileWriter('tensorflow/logdir', sess.graph)
            for epoch in range(epochs):
                total_loss = 0
                indices = list(range(n_train))
                r.shuffle(indices)

                for i in range(n_train // batch_size_train):
                    batch_indices = indices[i * batch_size_train: (i + 1) * batch_size_train]
                    batch_features = [train_set[i] for i in batch_indices]
                    batch_stances = [train_stances[i] for i in batch_indices]

                    batch_feed_dict = {features_pl: batch_features, stances_pl: batch_stances,
                                       keep_prob_pl: train_keep_prob}
                    _, current_loss = sess.run([opt_op, loss], feed_dict=batch_feed_dict)
                    total_loss += current_loss

                if save_data_for_plot:
                    # Predict train
                    train_feed_dict = {features_pl: train_set, keep_prob_pl: 1.0}
                    train_pred = sess.run(predict, feed_dict=train_feed_dict)
                    train_loss = total_loss
                    save_predictions(train_pred, 'data/predictions_train.csv')
                    train_accuracy, train_score = scorer.get_accuracy_and_score('data/predictions_train.csv',
                                                                                file_train_instances)

                    # Predict test
                    test_feed_dict = {features_pl: test_set, stances_pl: test_stances,
                                      keep_prob_pl: 1.0}
                    # _, test_loss = sess.run([opt_op, loss], feed_dict=test_feed_dict)
                    test_loss = 0

                    test_feed_dict = {features_pl: test_set, keep_prob_pl: 1.0}
                    test_pred = sess.run(predict, feed_dict=test_feed_dict)

                    save_predictions(test_pred, file_predictions)
                    test_accuracy, test_score = scorer.get_accuracy_and_score(file_predictions, 'data/test.csv')
                    print('epoch {} of {} with train-loss: {} and test-loss: {} and accuracy: {}'.format(epoch + 1,
                                                                                                         epochs,
                                                                                                         total_loss,
                                                                                                         test_loss,
                                                                                                         test_accuracy))
                    # save into file
                    plot_data.seek(0, 2)
                    plot_data.writelines("\r")
                    plot_data.writelines(
                        ','.join([str(epoch + 1),
                                  str(test_loss),
                                  str(train_loss),
                                  str(test_accuracy),
                                  str(train_accuracy),
                                  str(test_score),
                                  str(train_score)
                                  ]))
                else:
                    print('epoch {} of {} with loss: {}'.format(epoch + 1, epochs, total_loss))

            # Predict
            test_feed_dict = {features_pl: test_set, keep_prob_pl: 1.0}
            test_pred = sess.run(predict, feed_dict=test_feed_dict)

    # Save predictions
    save_predictions(test_pred, file_predictions)

    # execute scorer
    score_results = scorer.score(file_predictions, 'data/test.csv')
    overall_results.append((score_results))
    print(score_results)

# plot
if save_data_for_plot:
    plotter.plot(filename_plot_data, test_lang)

# save in file
file.save_txt('results/tr-{}_te-{}_sp-{}_{}-{}-{}-{}.txt'.format(train_lang, test_lang, test_split,  now.day, now.hour, now.minute, now.second),
              '-------- config -----------\n' +
              'train_lang:\t\t\t' + str(train_lang) + '\n' +
              'test_lang:\t\t\t' + str(test_lang) + '\n' +
              'lim_unigram:\t\t' + str(lim_unigram) + '\n' +
              'target_size:\t\t' + str(target_size) + '\n' +
              'hidden_size:\t\t' + str(hidden_size) + '\n' +
              'train_keep_prob:\t' + str(train_keep_prob) + '\n' +
              'l2_alpha:\t\t\t' + str(l2_alpha) + '\n' +
              'learn_rate:\t\t\t' + str(learn_rate) + '\n' +
              'clip_ratio:\t\t\t' + str(clip_ratio) + '\n' +
              'batch_size_train:\t' + str(batch_size_train) + '\n' +
              'epochs:\t\t\t\t' + str(epochs) + '\n' +
              'test_split:\t\t\t' + str(test_split) + '\n' +
              'crossfolds:\t' + str(crossfolds) + '\n' +
              'file_train_instances:\t' + str(file_train_instances) + '\n' +
              'file_test_instances:\t' + str(file_test_instances) + '\n' +
              'file_plot_data:\t' + str(file_plot_data) + '\n' +
              'note:\t\t\t' + str(note) + '\n' +
              '\n\n-------- results -----------\n' +
              str(overall_results)
              )
