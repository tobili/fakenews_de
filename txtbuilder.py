import os

import file

filename = 'train_bodies'
file_type = 'body'  # stance or body

input_path = 'data/de/{}.csv'.format(filename)
output_path = 'data/de/txt/{}'.format(filename)

data = file.read(input_path)

if not os.path.exists(output_path):
    os.makedirs(output_path)

for i, line in enumerate(data):
    text, line_id = None, None
    if file_type == 'stance':
        text = line['Headline']
        line_id = i
    elif file_type == 'body':
        text = line['articleBody']
        line_id = line['Body ID']
    file.save_txt('{}/{}.txt'.format(output_path, line_id), text)
