import os

import matplotlib.pyplot as plt

import file


def save_plot(title, ylabel, xtest, xtrain, epochs, file_name, lang, show=False, xmean=None):
    fig = plt.figure(figsize=(10, 6))

    line2 = plt.plot(epochs, xtrain, label='Train {}'.format(ylabel.lower()))
    plt.setp(line2, 'color', 'b', 'linewidth', 1.0)

    line1 = plt.plot(epochs, xtest, label='Test {}'.format(ylabel.lower()))
    plt.setp(line1, 'color', 'r', 'linewidth', 1.0)

    if xmean:
        mean = plt.plot(epochs, xmean, label='Test mean')
        plt.setp(mean, 'color', 'y', 'linewidth', 1.0)

    plt.xlim(0)

    plt.title(lang.upper() + ' ' + title.capitalize())
    plt.xlabel('Epochs')
    plt.ylabel(ylabel.capitalize())
    plt.tight_layout()
    plt.legend(loc='lower right', fontsize=9)
    if not os.path.isdir('data/plot_data/{}/'.format(file_name)):
        os.makedirs('data/plot_data/{}/'.format(file_name))
    fig.savefig('data/plot_data/{}/{}.png'.format(file_name, title.lower()), dpi=72)
    if show:
        plt.show()
    plt.close()


def compare_plots(title, ylabel_list, xdata_list, epochs, file_name, lang, show=False, xmean=None):
    COLORS = ['#EE3224', '#F78F1E', '#FFC222', '#DBC474', '#c2db74', '#74db9d', '#74c9db', '#74a9db', '#7485db',
              '#db748c']
    fig = plt.figure(figsize=(10, 6))
    for i, data in enumerate(xdata_list):
        line1 = plt.plot(epochs, data, label='Test {}'.format(ylabel_list[i].lower()))
        plt.setp(line1, 'color', COLORS[i], 'linewidth', 1.0)

    plt.xlim(0)

    plt.title(lang.upper() + ' ' + title.capitalize())
    plt.xlabel('Epochs')
    plt.ylabel('Score')
    plt.tight_layout()
    plt.legend(loc='lower right', fontsize=9)
    if not os.path.isdir('data/plot_data/{}/'.format(file_name)):
        os.makedirs('data/plot_data/{}/'.format(file_name))
    fig.savefig('data/plot_data/{}/{}.png'.format(file_name, title.lower()), dpi=72)
    if show:
        plt.show()
    plt.close()


def get_mean_line(data_list):
    data_sum = sum(float(i) for i in data_list) / len(data_list)
    return [data_sum] * len(data_list)


def plot(file_name, file_name2, lang):
    file_path = 'data/plot_data/{}.csv'.format(file_name)

    datas = file.read(file_path)

    acc = {'train': [], 'test': []}
    loss = {'train': [], 'test': []}
    score = {'train': [], 'test': []}
    epochs = []
    for data in datas:
        epochs.append(data['Epoch'])
        loss['test'].append(data['TestLoss'])
        loss['train'].append(data['TrainLoss'])

        acc['test'].append(data['TestAccuracy'])
        acc['train'].append(data['TrainAccuracy'])

        score['test'].append(data['TestScore'])
        score['train'].append(data['TrainScore'])

    file_path2 = 'data/plot_data/{}.csv'.format(file_name2)

    datas2 = file.read(file_path2)

    acc2 = {'train': [], 'test': []}
    loss2 = {'train': [], 'test': []}
    score2 = {'train': [], 'test': []}
    for data in datas2:
        loss2['test'].append(data['TestLoss'])
        loss2['train'].append(data['TrainLoss'])

        acc2['test'].append(data['TestAccuracy'])
        acc2['train'].append(data['TrainAccuracy'])

        score2['test'].append(data['TestScore'])
        score2['train'].append(data['TrainScore'])



    #save_plot('accuracy - optimized', 'accuracy', acc['test'], acc['train'], epochs, file_name, lang,
    #          xmean=get_mean_line(acc['test']))
    #save_plot('cross entropy loss - optimized', 'loss', loss['test'], loss['train'], epochs, file_name, lang)
    #save_plot('score - optimized', 'score', score['test'], score['train'], epochs, file_name, lang,
     #         xmean=get_mean_line(score['test']))

    compare_plots('score - compare', ['score - optimized', 'score - baseline'], [score['test'],score2['test']] , epochs, file_name, lang, show=True)


if __name__ == '__main__':
    file_path = 'de_16-19-43'
    file_path2 = 'de_16-22-15'
    plot(file_path, file_path2, 'de')
