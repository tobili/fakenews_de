-------- config -----------
train_lang:			de
test_lang:			de
lim_unigram:		5000
target_size:		4
hidden_size:		100
train_keep_prob:	0.6
l2_alpha:			1e-05
learn_rate:			0.01
clip_ratio:			5
batch_size_train:	500
epochs:				90
test_split:			0
file_train_instances:	data/de/train_stances.csv
file_test_instances:	data/de/test_stances.csv
note:			de with plot infos
file_plot_data:	data/plot_data/de_13-12:49.csv


-------- results -----------
CONFUSION MATRIX:
-------------------------------------------------------------
|           |   agree   | disagree  |  discuss  | unrelated |
-------------------------------------------------------------
|   agree   |    17     |     0     |    34     |     4     |
-------------------------------------------------------------
| disagree  |    12     |     1     |    28     |     9     |
-------------------------------------------------------------
|  discuss  |     1     |     0     |     2     |     0     |
-------------------------------------------------------------
| unrelated |     5     |     0     |    10     |    45     |
-------------------------------------------------------------
ACCURACY: 0.387
MAX  - the best possible score (100% accuracy)
NULL - score as if all predicted stances were unrelated
TEST - score based on the provided predictions

||    MAX    ||    NULL   ||    TEST   ||
||   123.0   ||   15.0    ||   50.0    ||

Score in percentage: 40.650% 
