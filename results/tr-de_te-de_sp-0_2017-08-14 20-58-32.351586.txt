-------- config -----------
train_lang:			de
test_lang:			de
lim_unigram:		5000
target_size:		4
hidden_size:		100
train_keep_prob:	0.6
l2_alpha:			1e-05
learn_rate:			0.01
clip_ratio:			5
batch_size_train:	500
epochs:				90
test_split:			0
file_train_instances:	data/de/normal/modified/train_stances.csv
file_test_instances:	data/de/normal/modified/test_stances.csv
note:			/normal/modified de dataset 30% test
file_plot_data:	data/plot_data/de_14-20:44.csv


-------- results -----------
CONFUSION MATRIX:
-------------------------------------------------------------
|           |   agree   | disagree  |  discuss  | unrelated |
-------------------------------------------------------------
|   agree   |    293    |     7     |    89     |    10     |
-------------------------------------------------------------
| disagree  |    10     |    31     |    17     |     5     |
-------------------------------------------------------------
|  discuss  |    91     |    11     |    731    |    25     |
-------------------------------------------------------------
| unrelated |    50     |     3     |    86     |   9947    |
-------------------------------------------------------------
ACCURACY: 0.965
MAX  - the best possible score (100% accuracy)
NULL - score as if all predicted stances were unrelated
TEST - score based on the provided predictions

||    MAX    ||    NULL   ||    TEST   ||
||  3841.5   ||  2521.5   ||  3598.0   ||

Score in percentage: 93.661% 
