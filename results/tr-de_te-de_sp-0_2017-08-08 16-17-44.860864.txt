-------- config -----------
train_lang:			de
test_lang:			de
lim_unigram:		5000
target_size:		4
hidden_size:		100
train_keep_prob:	0.6
l2_alpha:			1e-05
learn_rate:			0.01
clip_ratio:			5
batch_size_train:	500
epochs:				90
test_split:			0
file_train_instances:	data/de/train_stances.csv
note:			baseline before negation


-------- results -----------
CONFUSION MATRIX:
-------------------------------------------------------------
|           |   agree   | disagree  |  discuss  | unrelated |
-------------------------------------------------------------
|   agree   |    11     |     1     |    39     |     4     |
-------------------------------------------------------------
| disagree  |     3     |     2     |    38     |     7     |
-------------------------------------------------------------
|  discuss  |     2     |     0     |     1     |     0     |
-------------------------------------------------------------
| unrelated |     0     |     0     |    21     |    39     |
-------------------------------------------------------------
ACCURACY: 0.315
MAX  - the best possible score (100% accuracy)
NULL - score as if all predicted stances were unrelated
TEST - score based on the provided predictions

||    MAX    ||    NULL   ||    TEST   ||
||   123.0   ||   15.0    ||   44.5    ||

Score in percentage: 36.179% 
