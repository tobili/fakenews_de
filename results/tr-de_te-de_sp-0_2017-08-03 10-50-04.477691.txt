-------- config -----------
train_lang:			de
test_lang:			de
lim_unigram:		5000
target_size:		4
hidden_size:		100
train_keep_prob:	0.6
l2_alpha:			1e-05
learn_rate:			0.01
clip_ratio:			5
batch_size_train:	500
epochs:				90
test_split:			0
file_train_instances:	data/de/train_stances.csv
note:                   same label proportion as in fnc-1-Dataset


-------- results -----------
CONFUSION MATRIX:
-------------------------------------------------------------
|           |   agree   | disagree  |  discuss  | unrelated |
-------------------------------------------------------------
|   agree   |     6     |     0     |     6     |     0     |
-------------------------------------------------------------
| disagree  |     1     |     0     |     5     |     0     |
-------------------------------------------------------------
|  discuss  |     1     |     0     |     2     |     0     |
-------------------------------------------------------------
| unrelated |     1     |     0     |    12     |    47     |
-------------------------------------------------------------
ACCURACY: 0.679
MAX  - the best possible score (100% accuracy)
NULL - score as if all predicted stances were unrelated
TEST - score based on the provided predictions

||    MAX    ||    NULL   ||    TEST   ||
||   36.0    ||   15.0    ||   23.0    ||

Score in percentage: 63.889% 
