-------- config -----------
train_lang:			de
test_lang:			de
lim_unigram:		5000
target_size:		4
hidden_size:		100
train_keep_prob:	0.6
l2_alpha:			1e-05
learn_rate:			0.01
clip_ratio:			5
batch_size_train:	500
epochs:				90
test_split:			0
crossfolds:	1
file_train_instances:	data/de/test_stances.csv
file_test_instances:	data/de/test_stances.csv
file_plot_data:	data/plot_data/de_15-14:46.csv
note:			testset as train- and testset


-------- results -----------
['CONFUSION MATRIX:
-------------------------------------------------------------
|           |   agree   | disagree  |  discuss  | unrelated |
-------------------------------------------------------------
|   agree   |    16     |     3     |    24     |    12     |
-------------------------------------------------------------
| disagree  |    17     |     8     |     9     |    16     |
-------------------------------------------------------------
|  discuss  |     2     |     0     |     0     |     1     |
-------------------------------------------------------------
| unrelated |    21     |    12     |    21     |     6     |
-------------------------------------------------------------
ACCURACY: 0.179
MAX  - the best possible score (100% accuracy)
NULL - score as if all predicted stances were unrelated
TEST - score based on the provided predictions

||    MAX    ||    NULL   ||    TEST   ||
||   123.0   ||   15.0    ||   39.25   ||

Score in percentage: 31.911% 
']