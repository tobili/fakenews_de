-------- config -----------
train_lang:			de
test_lang:			de
lim_unigram:		5000
target_size:		4
hidden_size:		100
train_keep_prob:	0.6
l2_alpha:			1e-05
learn_rate:			0.01
clip_ratio:			5
batch_size_train:	500
epochs:				90
test_split:			0
file_train_instances:	data/de/train_stances.csv
note:                   train and testset: less fatures only: neg_overlap, neg_hyp_diff


-------- results -----------
CONFUSION MATRIX:
-------------------------------------------------------------
|           |   agree   | disagree  |  discuss  | unrelated |
-------------------------------------------------------------
|   agree   |    12     |     0     |    38     |     5     |
-------------------------------------------------------------
| disagree  |     4     |     0     |    39     |     7     |
-------------------------------------------------------------
|  discuss  |     1     |     0     |     2     |     0     |
-------------------------------------------------------------
| unrelated |     2     |     0     |    17     |    44     |
-------------------------------------------------------------
ACCURACY: 0.339
MAX  - the best possible score (100% accuracy)
NULL - score as if all predicted stances were unrelated
TEST - score based on the provided predictions

||    MAX    ||    NULL   ||    TEST   ||
||  123.75   ||   15.75   ||   45.5    ||

Score in percentage: 36.768% 
